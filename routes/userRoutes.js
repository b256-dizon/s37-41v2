const express = require("express");
const router = express.Router();
const userController = require("../controllers/userControllers.js");
const auth = require("../auth.js");

// Route for checking if the user's email already exists in the database
router.post("/checkEmail", (req, res) => {

	userController.checkIfEmailExists(req.body).then(resultFromController => res.send(resultFromController));

});

// Route for user registration
router.post("/register", (req, res) => {

	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));

})

// Route for user authentication
router.post("/login", (req, res) => {

	userController.authenticateUser(req.body).then(resultFromController => res.send(resultFromController));

})

// Route for retrieving user details
router.get("/details", auth.verify, (req, res) => {

	/*
	const userData = {
		id: 1,
		email: hillary@mail.com,
		isAdmin: true
	};
	*/
	const userData = auth.decode(req.headers.authorization);

	/*
	{userId: 1}
	*/
	userController.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController));

});

// Router for Enrolling a User
router.post("/enroll", auth.verify, (req, res) => {

	const data = {
		
		// userID will be received from the request header
		userId: auth.decode(req.headers.authorization).id,

		courseId: req.body.courseId

	}

	userController.enrollUser(data).then(resultFromController => res.send(resultFromController));

})

module.exports = router;