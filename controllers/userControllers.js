// contains all the business logic and functions of our application
// The "User" variable is defined using a capitalized letter to indicate that what we are using is the "User" model for code readability
const User = require("../models/User.js");
const Course =  require ("../models/Course.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

// Check if the email already exists
/*
	Business Logic: 
	1. Use mongoose "find" method to find duplicate emails
	2. Use the "then" method to send a response back to the frontend application based on the result of the "find" method
*/
module.exports.checkIfEmailExists = (requestBody) => {

	// The result is sent back to the frontend via the "then" method found in the route file
	return User.find({email: requestBody.email}).then(result => {

		// The "find" method returns a record if a match is found
		if(result.length > 0) {

			return true;

		// No duplicate email found
		// The user is not yet registered in the database
		} else {

			return false;

		}
	})
};

// User Registration
/*
	Business Logic:
	1. Create a new User object using the mongoose model and the information from the request body
	2. Make sure that the password is encrypted
	3. Save the new User to the database
*/
module.exports.registerUser = (requestBody) => {

	// Creates a variable "newUser" and instantiates a new "User" object using the mongoose model
	// Uses the information from the request body to provide all the necessary information
	let newUser = new User({
		firstName: requestBody.firstName,
		lastName: requestBody.lastName,
		email: requestBody.email,
		mobileNo: requestBody.mobileNo,
		// 10 is the value provided as the number of "salt" rounds that the bcrypt algorithm will run in order to encrypt the password
		password: bcrypt.hashSync(requestBody.password, 10)
	})

	// Saves the created object to our database
	return newUser.save().then((user, err) => {

		// User registration failed
		if(err) {

			return false;

		// User registration successful
		} else {

			return true;

		}
	})
} 

// User authentication
/*
	Business Logic:
	1. Check the database if the user email exists
	2. Compare the password provided in the login form with the password stored in the database
	3. Generate/return a JSON web token if the user is successfully logged in and return false if not
*/
module.exports.authenticateUser = (requestBody) => {

	// The "findOne" method returns the first record in the collection that matches the search criteria
	// We use the "findOne" method instead of the "find" method which returns all records that match the search criteria
	return User.findOne({email: requestBody.email}).then(result => {

		// User does not exist
		if(result == null) {

			return false;

		// User exists
		} else {

			// Creates the variable "isPasswordCorrect" to return the result of comparing the login form password and the database password
			// The "compareSync" method is used to compare a non encrypted password from the login form to the encrypted password retrieved from the database and returns "true" or "false" value depending on the result
			// A good coding practice for boolean variable/constants is to use the word "is" or "are" at the beginning in the form of is+Noun
				//example. isSingle, isDone, isAdmin, areDone, etc..
			const isPasswordCorrect = bcrypt.compareSync(requestBody.password, result.password)

			// If the passwords match/result of the above code is true
			if(isPasswordCorrect) {

				// Generate an access token
				// Uses the "createAccessToken" method defined in the "auth.js" file
				// Returning an object back to the frontend application is common practice to ensure information is properly labeled and real world examples normally return more complex information represented by objects
				return {access: auth.createAccessToken(result)}

			// Passwords do not match
			} else {

				return false;
			};
		};
	});
};

// SESSION 38 ACTIVITY SOLUTION

// Retrieve user details
/*
	Steps:
	1. Find the document in the database using the user's ID
	2. Reassign the password of the returned document to an empty string
	3. Return the result back to the frontend
*/
/*
	data = {
	 	userId: userData.id
	}
*/
/*
	data = {
		userId: 1
	}
*/
module.exports.getProfile = (data) => {

	// findById(1)
	return User.findById(data.userId).then(result => {

		// Changes the value of the user's password to an empty string when returned to the frontend
		// Not doing so will expose the user's password which will also not be needed in other parts of our application
		// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
		result.password = "";

		// Returns the user information with the password as an empty string
		return result;

	});

};

// Enrolling a user to a course

/*
	Business Logic:
	1. Find the document in the database using the user's ID
	2. Add the course ID to the user's enrollment array
	3. Update the document in the MongoDB Atlas Database
*/

module.exports.enrollUser = async (data) => {

	let isUserUpdate = await User.findById(data.userId).then(user => {

		user.enrollments.push({courseId:data.courseId});

		return user.save().then((enrolled, err)=> {

			if (err){
				return false;
			} else{
				return true
			}

		})

	});

	Course.findById(data.courseId).then(course => {

		course.enrollees.push ({userId:data.userId});

		return course.save().then((enroll,err) => {

			if (err){

				return false;
			}	else {

				return true;

				}
			})
	});


let isCourseUpdate = await Course.findById(data.courseId).then(course => {

		course.enrollees.push ({userId:data.userId});

		return course.save().then((enroll,err) => {

			if (err){

				return false;
			}	else {

				return true;

				}
			})
	})

	if (isUserUpdate && isCourseUpdate) {

		return true;
	} else {

		return false;
	}


}